OBJS=explaining.pdf explaining.ps explaining.dvi

explaining.pdf: plan1.txt depesz.png tatiyants.png scan.txt

all: $(OBJS)

%.dvi: %.tex
	lualatex -shell-escape $<
%.pdf: %.tex $(wildcard *.png) $(wildcard *.yaml)
	lualatex -shell-escape $<
%.ps: %.pdf
	pdf2ps $< $@

clean:
	rm -f $(OBJS) *.aux *.log *.toc *.nav *.out *.snm *~


